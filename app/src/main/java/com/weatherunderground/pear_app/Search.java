package com.weatherunderground.pear_app;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.net.URL;
import java.net.URLEncoder;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Search {
    //jse is for all other class use
    private static JsonElement jse = null;
    private boolean resultsLong = false;
    private boolean resultsShort = false;
    private boolean errorUnknown = false;
    private JsonElement jseUno = null;

    /**
     * This will be Search Part of the Class
     * Completed by David Williams
     * This will get the Search information, as if it were an actual search on Weather Underground
     * It reads it from the first JSON query then will set the main JSE JSON element for the rest of the backend
     * This is the most important part of the class, should have no errors, as it had none as of 0.1.1a
     * Edited by:
     * David Williams
     * ### WARNING ###
     * DO NOT ADAPT THIS CODE IN ANY WAY
     * ### WARNING ###
     */

    public Search(String input) {


        final String WU_Key = "0e119c18b51b9ea5";
        try {
            /*
             Do a query to get suggestions, then later check to see if there is only one option
             if only one option then use that json file to go to next step to get the next json that
             will have the specific info we need for the app
             this will allow for there to be checks in this backend, rather than having to implement it
             all into the GUI
             */

            //Encode the search for URL Compatibility
            String firstEncoded = URLEncoder.encode(input, "utf-8");

            //Combine URL for next steps
            URL firstURL = new URL("http://autocomplete.wunderground.com/aq?query=" + firstEncoded);

            //Get InputStream
            InputStream isOne = firstURL.openStream();

            //Get BufferedReader
            BufferedReader brOne = new BufferedReader(new InputStreamReader(isOne));

            //add parsed (first) Json elements in order to complete search results into jseOne
            jseUno = new JsonParser().parse(brOne);


            isOne.close();
            brOne.close();

        } catch (java.io.IOException ioErr) {
            errorUnknown = true;
        }


        if (jseUno != null && !errorUnknown) {
            if (jseUno.getAsJsonObject().get("RESULTS").getAsJsonArray().size() > 1) {
                resultsLong = true;
            } else if (jseUno.getAsJsonObject().get("RESULTS").getAsJsonArray().size() < 1) {
                resultsShort = true;
            } else {


                /*
                 * try to get the exact area for the weather from search results
                 * this eliminated the need for error checking in the GUI
                 * Now get the final results for the actual weather outputs
                 */


                try {


                    //Encode the search for URL Compatibility
                    String secondEncoded = jseUno.getAsJsonObject().get("RESULTS").getAsJsonArray().get(0).getAsJsonObject().get("l").getAsString();

                    //Combine URL for next steps
                    URL secondURL = new URL("http://api.wunderground.com/api/" +
                            WU_Key + "/conditions/radar/forecast10day" + secondEncoded + ".json");

                    //Get InputStream
                    InputStream isTwo = secondURL.openStream();

                    //Get BufferedReader
                    BufferedReader brTwo = new BufferedReader(new InputStreamReader(isTwo));

                    //add parsed (first) Json elements in order to complete search results into jseOne
                    jse = new JsonParser().parse(brTwo);


                    isTwo.close();
                    brTwo.close();

                } catch (java.io.IOException ioErr) {
                    ioErr.printStackTrace();
                }


            }

        } else {
            errorUnknown = true;
        }


    }

    public Search() {

    }

    String returnLoc() {
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("display_location").getAsJsonObject().get("full").getAsString();
    }


    String date() {
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("observation_time").getAsString();
    }

    String errCheck() {
        if (resultsLong) {
            return "Please narrow your search results";
        } else if (resultsShort) {
            return "Your search came up with no results, please check for errors";
        } else if (errorUnknown) {
            return "Your search came up with an unknown error";
        } else {
            return "";
        }
    }


    /**
     * This will be CC Part of this Class
     * Completed by Ronghua Su
     * This will get the Current Conditions from the JSON Doc
     * This might need to be added to if we decide to add more data points on the GUI
     * Edited by:
     * David Williams (Merged) (EDITED)Removed getCity(); (EDITED) Moved getWind() to correct section;
     * Ronghua Su
     */

    String getCurrTemp() {
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("temp_f").getAsString();
    }

    String getWeather() {
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("weather").getAsString();
    }

    private String getIcon() {
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("icon").getAsString();
    }

    String getIconFilename() {
        String url = jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("icon_url").getAsString();
        String temp = url.substring(url.lastIndexOf("/")+1);
        return temp.substring(0, temp.indexOf('.'));
    }

    String getFeelsLike() {
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("feelslike_f").getAsString();
    }

    String getHumidity() {
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("relative_humidity").getAsString();
    }

    String getWind() {
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("wind_mph").getAsString();
    }



    /**
     * This will be Radar Part of this Class
     * Completed by Josh Delany
     * This will get the radar from the JSON Doc
     * This may change depending out what radar we want
     * Edited by:
     * David Williams (Merged)
     * Josh Delany
     */

    String getRadar() {
        try {
            return jse.getAsJsonObject().get("radar").
                    getAsJsonObject().get("image_url").getAsString();
        } catch (NullPointerException npe) {
            return "error";
        }
    }

    /**
     * This will be FC Part of this Class
     * Completed by David Williams
     * This will get the Forecast from the JSON Doc
     * This is adaptable as well
     * Edited by:
     * David Williams
     * Josh Delany ( getIcon getHigh getLow )
     */

    String getDate(int day) {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(day).getAsJsonObject().get("date").getAsJsonObject().get("weekday_short").getAsString()
                + " " + jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(day).getAsJsonObject().get("date").getAsJsonObject().get("monthname_short").getAsString()
                + " " + jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(day).getAsJsonObject().get("date").getAsJsonObject().get("day").getAsString();
    }

    String getIcon(int day) {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(day).getAsJsonObject().
                get("icon").getAsString();
    }

    String getHigh(int day) {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(day).getAsJsonObject().
                get("high").getAsJsonObject().get("fahrenheit").getAsString();
    }

    String getLow(int day) {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(day).getAsJsonObject().
                get("low").getAsJsonObject().get("fahrenheit").getAsString();
    }

    String getConditions(int day) {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(day).getAsJsonObject().
                get("conditions").getAsString();
    }

    //end of section

    /**
     * This will be the backend test method
     * Make sure there are NO errors in THIS code BEFORE committing
     * <p>
     * Last Edited by:
     * David Williams
     */
    public static void main(String[] args) {
        Search sh = new Search("Roseville,California");
        System.out.println(sh.errCheck());
        System.out.println(sh.returnLoc());
        System.out.println(sh.date());
        String st = Search.jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("display_location").getAsJsonObject().get("full").getAsString();
        System.out.println(st + "  TA DA");
        Search shh = new Search("Roseville,CA");
        System.out.println(shh.errCheck());
        Search s = new Search("Rosevillllle,CA");
        System.out.println(s.errCheck());

        System.out.println(sh.getRadar());

        System.out.println(sh.getCurrTemp());
        System.out.println(sh.getHumidity());
        System.out.println(sh.getWeather());
        System.out.println(sh.getIcon());
        System.out.println(sh.getWind());

        System.out.println(sh.getFeelsLike());
        System.out.println(sh.getDate(1));
        System.out.println(sh.getHigh(1));
        System.out.println(sh.getLow(1));
        System.out.println(sh.getIcon(1));

        Search srh = new Search("95661");
        System.out.println(srh.returnLoc());
    }

}