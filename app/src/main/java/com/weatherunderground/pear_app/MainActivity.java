package com.weatherunderground.pear_app;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void getWeather(View v) {
        // Get the text from the input field
        EditText location = (EditText) findViewById(R.id.ZipBox);

        String zip = location.getText().toString();
        new GetWeatherInBackground().execute(zip);
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }



    private class GetWeatherInBackground extends AsyncTask<String, Void, Search>
    {
        @Override
        protected Search doInBackground(String... locations)
        {
            // Fetch the weather data
            Search w = new Search(locations[0]);
            if (isNetworkConnected()) {
                return w;
            }
            else
            {
                return new Search("error");
            }
        }

        @Override
        protected void onPostExecute(Search m) {
            if (m.errCheck().equals("")){


                //ERROR CHECKING FEATURE
                TextView temp = (TextView) findViewById(R.id.ErrText);
                temp.setText(m.errCheck());


                //*****Current Conditions*****
                // Current Temp
                temp = (TextView) findViewById(R.id.TempText);
                temp.setText(m.getCurrTemp() + "\u00B0F");

                // Location
                temp = (TextView) findViewById(R.id.CityText);
                temp.setText(m.returnLoc());

                // Feels Like
                temp = (TextView) findViewById(R.id.FeelsLikeText);
                temp.setText("Feels Like " + m.getFeelsLike() + "\u00B0F");

                // Humidity
                temp = (TextView) findViewById(R.id.HumidityText);
                temp.setText(m.getHumidity());

                // Wind
                temp = (TextView) findViewById(R.id.WindText);
                temp.setText(m.getWind() + " mph");

                // Weather Conditions
                temp = (TextView) findViewById(R.id.ConditionsText);
                temp.setText(m.getWeather());

                // Change Icon
                ImageView v = (ImageView) findViewById(R.id.ConditionsIcon);
                Context context = v.getContext();
                v.setImageResource(context.getResources().getIdentifier(m.getIconFilename(), "drawable", context.getPackageName()));

                //************Radar***********
                WebView radar = (WebView) findViewById(R.id.RadarView);
                radar.loadUrl(m.getRadar());

                //**********Forecast**********

                //Day0
                temp = (TextView) findViewById(R.id.Day0);
                temp.setText(m.getDate(0));

                temp = (TextView) findViewById(R.id.High0);
                temp.setText("High of " + m.getHigh(0) + "\u00B0F");

                temp = (TextView) findViewById(R.id.Low0);
                temp.setText("Low of " + m.getLow(0) + "\u00B0F");

                temp = (TextView) findViewById(R.id.conditions0);
                temp.setText(m.getConditions(0));

                v = (ImageView) findViewById(R.id.img0);
                v.setImageResource(context.getResources().getIdentifier(m.getIcon(0), "drawable", context.getPackageName()));
                //
                //Day1
                temp = (TextView) findViewById(R.id.Day1);
                temp.setText(m.getDate(1));

                temp = (TextView) findViewById(R.id.High1);
                temp.setText("High of " + m.getHigh(1) + "\u00B0F");

                temp = (TextView) findViewById(R.id.Low1);
                temp.setText("Low of " + m.getLow(1) + "\u00B0F");

                temp = (TextView) findViewById(R.id.conditions1);
                temp.setText(m.getConditions(1));

                v = (ImageView) findViewById(R.id.img1);
                v.setImageResource(context.getResources().getIdentifier(m.getIcon(1), "drawable", context.getPackageName()));
                //
                //Day2
                temp = (TextView) findViewById(R.id.Day2);
                temp.setText(m.getDate(2));

                temp = (TextView) findViewById(R.id.High2);
                temp.setText("High of " + m.getHigh(2) + "\u00B0F");

                temp = (TextView) findViewById(R.id.Low2);
                temp.setText("Low of " + m.getLow(2) + "\u00B0F");

                temp = (TextView) findViewById(R.id.conditions2);
                temp.setText(m.getConditions(2));

                v = (ImageView) findViewById(R.id.img2);
                v.setImageResource(context.getResources().getIdentifier(m.getIcon(2), "drawable", context.getPackageName()));
                //
                //Day3
                temp = (TextView) findViewById(R.id.Day3);
                temp.setText(m.getDate(3));

                temp = (TextView) findViewById(R.id.High3);
                temp.setText("High of " + m.getHigh(3) + "\u00B0F");

                temp = (TextView) findViewById(R.id.Low3);
                temp.setText("Low of " + m.getLow(3) + "\u00B0F");

                temp = (TextView) findViewById(R.id.conditions3);
                temp.setText(m.getConditions(3));

                v = (ImageView) findViewById(R.id.img3);
                v.setImageResource(context.getResources().getIdentifier(m.getIcon(3), "drawable", context.getPackageName()));
                //
                //Day4
                temp = (TextView) findViewById(R.id.Day4);
                temp.setText(m.getDate(4));

                temp = (TextView) findViewById(R.id.High4);
                temp.setText("High of " + m.getHigh(4) + "\u00B0F");

                temp = (TextView) findViewById(R.id.Low4);
                temp.setText("Low of " + m.getLow(4) + "\u00B0F");

                temp = (TextView) findViewById(R.id.conditions4);
                temp.setText(m.getConditions(4));

                v = (ImageView) findViewById(R.id.img4);
                v.setImageResource(context.getResources().getIdentifier(m.getIcon(4), "drawable", context.getPackageName()));
                //

                //****************************

                //***Make everything visible***

                // Make forecast visible
                LinearLayout lay = (LinearLayout) findViewById(R.id.ForecastSection);
                lay.setVisibility(View.VISIBLE);

                // Make Current Conditions visible
                lay = (LinearLayout) findViewById(R.id.ConditionsSection);
                lay.setVisibility(View.VISIBLE);

                // Make current temp, feelslike, and location visible.
                temp = (TextView) findViewById(R.id.FeelsLikeText);
                temp.setVisibility(View.VISIBLE);

                temp = (TextView) findViewById(R.id.TempText);
                temp.setVisibility(View.VISIBLE);

                temp = (TextView) findViewById(R.id.CityText);
                temp.setVisibility(View.VISIBLE);

                //****************************
            }
            else{
                TextView temp = (TextView) findViewById(R.id.ErrText);

                temp.setText(m.errCheck());
            }
        }
    }
}
